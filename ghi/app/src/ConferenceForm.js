import React from 'react'
import { useEffect, useState } from 'react'

// add try, except block
export default function ConferenceForm() {
    // condense handling of form data input from user
      const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    })
    // save changes to form state
    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name

        setFormData({
            ...formData,
            [inputName]:value,
        })
    }

    // get locations to populate dropdown
    const [locations, setLocations] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(() => {fetchData()}, [])

    // handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: 'post',
            // can pass formData in directly because we are using state object
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            // can also clear data directly
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            })
        }
    }


    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Conference</h1>
            {/* <!-- create conference form --> */}
            <form onSubmit={handleSubmit} id="create-conference-form">
                {/* <!-- name --> */}
              <div className="form-floating mb-3">
                <input onChange ={handleFormChange} value ={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              {/* <!-- starts --> */}
              <div className="form-floating mb-3">
                <input onChange ={handleFormChange} value ={formData.starts} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control" />
                <label htmlFor="starts">Start Date</label>
              </div>
              {/* <!-- ends --> */}
              <div className="form-floating mb-3">
                <input onChange ={handleFormChange} value ={formData.ends} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="starts">End Date</label>
              </div>
              {/* <!-- max_presentations --> */}
              <div className="form-floating mb-3">
                <input onChange ={handleFormChange} value ={formData.max_presentations} placeholder="Max presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              {/* <!-- max_attendees --> */}
              <div className="form-floating mb-3">
                <input onChange ={handleFormChange} value ={formData.max_attendees} placeholder="Max attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              {/* <!-- location --> */}
              <div className="mb-3">
                <select onChange ={handleFormChange} value ={formData.location} required id="location" name="location" className="form-select">
                  <option value="">Conference Location</option>
                  {
                    locations.map(location => {
                        return (
                            <option value={location.id} key={location.href}>
                                {location.name}
                            </option>
                        )
                    })
                  }
                </select>
              </div>
              {/* description */}
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange ={handleFormChange} value ={formData.description} required id="description" name="description" className="form-control" rows="3"></textarea>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}
