import React from 'react'
import { useEffect, useState } from 'react'


export default function LocationForm() {

    const [name, setName] = useState('')
    const [roomCount, setRoomCount] = useState('')
    const [city, setCity] = useState('')
    const [state, setState] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleRoomChange = (event) => {
        const value = event.target.value
        setRoomCount(value)
    }
    const handleCityChange = (event) => {
        const value = event.target.value
        setCity(value)
    }
    const handleStateChange = (event) => {
        const value = event.target.value
        setState(value)
    }
    // handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.room_count = roomCount
        data.city = city
        data.state = state

        // handle POST request to send form data
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)

            // set the state to empty strings so form "reloads"
            setName('')
            setRoomCount('')
            setCity('')
            setState('')
        }
    }

    // get the states data to populate dropdown
    const [states, setStates] = useState([])
    const fetchData = async () => {
        try{
            const url = 'http://localhost:8000/api/states/'
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setStates(data.states)
            }
        }
        catch (e) {
            console.log('error', e)
        }
    }
    // useEffect hook to call fethcData/ get states data when page initally loads
    useEffect(() => {
        fetchData()
    },[])

    // return JSX
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit = {handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange ={handleNameChange} value={name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomChange} value = {roomCount} placeholder="Room count" required type="number" id="room_count" name="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange= {handleCityChange} value = {city} placeholder="City" required type="text" id="city" name="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange= {handleStateChange} value = {state} required id="state" name="state" className="form-select">
                <option value="">Choose a state</option>
                  {
                  states.map(state => {
                        return (
                            <option value={state.abbreviation} key={state.abbreviation}>
                                {state.name}
                            </option>
                        )
                    })
                  }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

                // this line ^ changes the State of "states" from an empty array
                // to an array filled with the info from data.states
                //  (console.log(data) showed that the data from response is stored in states property)
                // initially tried setStates(states= data.states)
                // got an error for trying to assign a const variable
