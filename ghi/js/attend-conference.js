window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference')
    const url = 'http://localhost:8000/api/conferences/ '
    const response = await fetch(url)
    if (response.ok) {
        // load conferences into dropdown menu
        const data = await response.json()
        console.log(data)
        for (let conference of data.conferences) {
            const option = document.createElement('option')
            option.value = conference.href
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        // add d-none class to loading icon to hide it
        const loadingTag = document.getElementById('loading-conference-spinner')
        loadingTag.classList.add('d-none')
        // remove d-none class from conference select tag so dropdown appears
        selectTag.classList.remove('d-none')

        // get data from form submission
        const formTag = document.getElementById('create-attendee-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            // create FormData object with form data
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))

            // send data to server (attendees as POST request)
            const attendeesUrl = 'http://localhost:8001/api/attendees/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const response = await fetch(attendeesUrl, fetchConfig)

            // add d-none class to form tag so its hidden once form is submitted
            formTag.classList.add('d-none')
            // remove d-none class from success message
            const successTag = document.getElementById('success-message')
            successTag.classList.remove('d-none')

        })
    }
})
