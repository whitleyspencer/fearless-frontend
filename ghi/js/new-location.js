//add event listener for when the DOM loads
window.addEventListener("DOMContentLoaded", async () => {
    const url = 'http://localhost:8000/api/states/'
    // get data from state list api to populate states in form
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        const select = document.getElementById('state')
        // loop through the states in data (looping through list)
        for (let state of data.states) {
            const newOption = document.createElement("option")
            newOption.value = state.abbreviation
            newOption.innerHTML = state.name
            select.appendChild(newOption)
        }

        // get data from form submission
        const formTag = document.getElementById('create-location-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            // create FormObject from form element
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            // send data to server
            const locationUrl = 'http://localhost:8000/api/locations/'
            const fetchConfig = {
              method: "post",
              body: json,
              headers: {
                'Content-Type': 'application/json',
              },
            }
            const response = await fetch(locationUrl, fetchConfig)
            if (response.ok) {
              formTag.reset()
              const newLocation = await response.json()
            //   console.log(newLocation)
            }
        })

    } else {
        throw new Error("response not ok")
    }
})





// when the page loads, call states RESTful API
// get the data back
// loop through the data
// for each state,
// create an <option> element that has the abbrev and state name


// handle submission of form and get its data:
// take the form element and create a new FormData object from it
//      form element is stored in variable formTag
//          --> we added the event listener to it already
