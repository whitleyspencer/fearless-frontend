function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="card shadow mb-4 bg-white">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
            <p>${starts.toLocaleDateString()} - ${ends.toLocaleDateString()}<p>
        </div>
    </div>

  `
}

function alert(message) {
    return `
    <div class="alert alert-dark alert-dismissible" role="alert">
        ${message}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
`
}

// this is an event handler for when the DOM content has loaded
// fetch the conference data from the API
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'
    try {
        const response = await fetch(url)

        if (!response.ok) {
            // what to do when the response is bad
            const alertHtml = alert("bad response")
            const alertNotice = document.querySelector('#liveAlertPlaceholder')
            alertNotice.innerHTML += alertHtml
            throw new Error('Response not ok')
        } else {
            // ie response is ok:

            // get data from list conferences request
            const data = await response.json()

            // write a for-of loop to loop over the list of conferences and populate the card
            // with the details of each conference in the list
            let i = 0
            for (let conference of data.conferences) {

                // get the data from the conference detail request
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)

                if (detailResponse.ok) {
                    // the variable details is the conference detail data --> use it to access
                    //      name, description, and picture_url
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const starts = new Date(details.conference.starts)
                    const ends = new Date(details.conference.ends)
                    const location = details.conference.location.name


                    // set the variable html equal to the function createCard
                    const html = createCard(title, description, pictureUrl, starts, ends, location)
                    // this will return the html with each of the conferences information

                    // get the cards into columns on the website
                    const column = document.querySelector(`#col${i% 3}`)
                    column.innerHTML += html
                    i++
                }

            }
        }
    } catch (e) {
        // what to do if error is raised
        console.error('error', e)
    }
});
