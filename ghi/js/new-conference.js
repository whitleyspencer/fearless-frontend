console.log("hello world")

window.addEventListener("DOMContentLoaded", conferenceForm)




async function conferenceForm() {
    // load form with locations in dropdown
    const url = 'http://localhost:8000/api/locations'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        const selectElement = document.getElementById('location')
        for (let location of data.locations) {
            const newOption = document.createElement('option')
            newOption.value = location.id
            newOption.innerHTML = `${location.name} - ${location.city}`
            selectElement.appendChild(newOption)
        }

        // get data from form submission
        // send back to server with POST request
        const formTag = document.getElementById('create-conference-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            // create FormObject
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))

            // send data from FormObject to server via POST request
            const conferenceURL = 'http://localhost:8000/api/conferences/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                },
            }
            const response = await fetch(conferenceURL, fetchConfig)
            if (response.ok) {
                formTag.reset()
                const newConference = await response.json()
                console.log(newConference)
            }


        })
    } else {
        throw new Error('response not ok')
    }
}
